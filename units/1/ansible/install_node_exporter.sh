#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  SUDOPREFIX="sudo"
else
  SUDOPREFIX=""
fi

# Need ansible
${SUDOPREFIX} apt update && ${SUDOPREFIX} apt install -y ansible

# Run playbook
cd "$( dirname "$0" )"
ansible-playbook playbook.yml
