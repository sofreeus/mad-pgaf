# Exporters & Querying

## Overview

These definitions comes from the Google SRE book[^srebook]:

### Whitebox Monitoring

> Monitoring based on metrics exposed by the internals of the system, including logs, interfaces like the Java Virtual Machine Profiling Interface, or an HTTP handler that emits internal statistics.

### Blackbox Monitoring

> Testing externally visible behavior as a user would see it.

David's got a whole unit on Blackbox monitoring after this one.

### Exporters

Prometheus has a large number of exporters for whitebox monitoring[^promintegrations]. Node Exporter is a popular one!

Node Exporter has a built in collector that will read textfiles for metrics. This is specified with `--collector.textfile.directory` flag. In our ansible playbook, we've specified the directory `/var/lib/node_exporter/textfile_collector`.[^textfile]

### Querying

Querying the TSDB is done using the Prometheus Query Language (PromQL)[^promquerying].

* Instant vector - a set of time series containing a single sample for each time series, all sharing the same timestamp
* Range vector - a set of time series containing a range of data points over time for each time series
* Scalar - a simple numeric floating point value

```
# Instant vector
prometheus_http_requests_total

# Range vector
prometheus_http_requests_total[5m]

# Average rate of requests over 5 minutes
rate(prometheus_http_requests_total[5m])

# Filtering
prometheus_http_requests_total{code="200"}

# Math
100 * sum(prometheus_http_requests_total{code!="200"}) / sum(prometheus_http_requests_total)
```

### Service Discovery

In this class, we will be configuring Prometheus with static targets, but that's not very scalable in a dynamic cloud environment! Prometheus can use a variety of data sources to discover targets. Those may include:

* Kubernetes
* Consul
* AWS API
* OpenStack API
* DNS
* A simple file
* So much more![^promconfig]

## Demo

1. Run the ansible playbook to install Node Exporter
2. Configure Prometheus to scrape node exporter
3. Run a query on `up`
4. Run a query showing CPU usage

## Cheat sheet

To install node exporter, run the script `units/1/ansible/install_node_exporter.sh`.

Add this to the bottom of `/srv/mad-stack/prometheus/prometheus.yml` and then run `cd /srv/mad-stack && docker-compose restart prometheus`:

```yaml
  - job_name: 'node_exporter'
    static_configs:
    - targets:
      - '$MACGUFFIN.sofree.us:9100'
```

CPU query: `100 - (avg by (instance) (irate(node_cpu_seconds_total{mode="idle"}[5m])) * 100)`

## References

[^srebook]: [Google SRE Book | Chapter 6 - Monitoring Distributed Systems](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/)
[^promintegrations]: [Prometheus | Exporers and Integration](https://prometheus.io/docs/instrumenting/exporters/)
[^textfile]: An example of using textfile collector [is here](https://www.robustperception.io/using-the-textfile-collector-from-a-shell-script)
[^promquerying]: [Prometheus | Querying Basics](https://prometheus.io/docs/prometheus/latest/querying/basics/)
[^promconfig]: [Prometheus | Configuration](https://prometheus.io/docs/prometheus/latest/configuration/configuration/)
