# Overview and Infrastructure

![Prometheus, Grafana, and friends](MAD.png)

## Talk

- What is metrics-based monitoring, and why is it better than check-based monitoring?
- What is an Exporter?
	* Examples: Node, WMI, Blackbox
	* Example of a metric:
	```
	# HELP node_load1 1m load average.
	# TYPE node_load1 gauge
	node_load1 0.03
	```
- What is Prometheus?
	* Why HTTP scrapes? (pull-based v push-based)
- What is Grafana?
	* Node Dashboard
	* WMI Dashboard
- data flow
	* Exporters publish
	* Prometheus scrapes Exporters
	* Prometheus notifies Alertmanager when a rule is matched
	* Grafana pulls from Prometheus on demand

## Demo

Let's stand up a stack of containers that can do monitoring, alerting, and dashboards. We'll call it ... a MAD stack.

Use [GLADOS](https://gitlab.com/sofreeus/glados/-/blob/main/HOWTO.md) to create your MAD host
- The instructors will need your macguffin and IP address to set up DNS

Get class materials

Download the [git-a-class](https://gitlab.com/sofreeus/mad-pgaf/-/tree/master/units/0/git-a-class) script and copy it to your host

```bash
scp git-a-class $MACGUFFIN.sofree.us: # don't forget the trailing colon
```

Log in to MAD host and boot the stack

```bash
ssh $MACGUFFIN.sofree.us
# If your local username doesn't match, use this form
# ssh $USERNAME@$MACGUFFIN.sofree.us
```
```bash
bash git-a-class mad-pgaf
cd ~/sfs/mad-pgaf/units/0/mad-stack/
./mad-stack-host-prep
# log back in
cd ~/sfs/mad-pgaf/units/0/mad-stack/
./pave
./boot
```

Open browser tabs on Prometheus, Grafana, and friends

  - prometheus.$MACGUFFIN.sofree.us
  - grafana.$MACGUFFIN.sofree.us
  - alertmanager.$MACGUFFIN.sofree.us
  - blackbox_exporter.$MACGUFFIN.sofree.us

## Pair (same exercise as Demo)
