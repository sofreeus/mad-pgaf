# Alerting

## Overview

Gathering metrics is useful. They're even more useful if you get notified when metrics go wonky.

Alertmanager does more than just send alerts. It can do things like:

* Group - Categorize similar alerts together into a single notification.
* Inhibit - Don't alert that a service is broken if the cluster the service is on is broken too.
* Silence - Selectively mute alerts for a given time.

Emails are useful, but I get too many e-mails. Some more useful ways:

* PagerDuty, OpsGenie, VictorOps, and others.
* Slack, Mattermost, AKA, my preferrable way of communicating.
* Your own custom webhooks.
* And so much more! [^alertmanager]

## Demo

### Configure Alertmanager

You can use the "Slack" receiver to have Alertmanager send alerts to Mattermost[^notifications]. A webhook has been configured for this class.

Point Alertmanager to Mattermost.

1. Edit `/srv/mad-stack/alertmanager/alertmanager.yml`.
1. Change the `receiver` line from `'web.hook'` to `'mattermost'`.
1. Remove the receiver config for `web.hook` and add a new config for `mattermost`.
    ```yaml
    ...
      reciever: 'mattermost'

    receivers:
      - name: mattermost
        slack_configs:
        - api_url: <mattermost_webhook_url>
          send_resolved: true
          pretext: "<Your Name>'s Prometheus"
          text: "summary: {{ .CommonAnnotations.summary }}\ndescription: {{ .CommonAnnotations.description }}"
    ```
1. Restart alertmanager.
   ```bash
   cd /srv/mad-stack/ ; docker-compose restart alertmanager
   ```

### Configure Prometheus

1. Copy the [rules.yml](rules.yml) file to Prometheus' configuration folder
    ```bash
    cp ~/sfs/mad-pgaf/units/3/rules.yml /srv/mad-stack/prometheus/rules.yml
    ```
1. Add the file to the `rule_files` section of the prometheus configuration `/srv/mad-stack/prometheus/prometheus.yml`.
    ```yaml
    rule_files:
      - "rules.yml"
    ```
1. Set your alertmanager configuration in `prometheus.yml` to look like this:
    ```yaml
    # Alertmanager configuration
    alerting:
      alertmanagers:
      - static_configs:
        - targets:
          - alertmanager:9093
    ```
1. Restart prometheus
   ```bash
   cd /srv/mad-stack/ ; docker-compose restart prometheus
   ```
1. Inspect the Prometheus and Alertmanager UI to make sure your configurations are there.

### Generate an alert and fix it

1. Stop node exporter on your host.
    ```bash
    sudo systemctl stop node_exporter
    ```
1. Wait for the alert to fire (2 minutes).
1. Start node exporter on your host.
    ```bash
    sudo systemctl start node_exporter
    ```
1. Wait for the alert to resolve.

## References

[^alertmanager]: [Alertmanager configuration](https://prometheus.io/docs/alerting/latest/configuration/)

[^notifications]: [Notification template examples](https://prometheus.io/docs/alerting/latest/notification_examples/)
