# Grafana

## Grafana's Mission: Democratize Metrics!

![graphs](https://grafana.com/static/img/grafana/showcase_visualize-954.jpg)

## What is Grafana?

* Open-source platform for visualizing metrics from many sources (Prometheus, ElasticSearch, PostgreSQL, cloud providers)
* Focused on visibility of different technologies, to create a full picture of your applications or systems

## Why?

Make correlations between systems

* DB "slow"? Pair together metrics such as read latency with disk IO, CPU, connection count to find the culprit
* Blackbox + Whitebox monitoring together
* Application crashing, no recent changes
* Visualize the overall health of multiple sources

See historical metrics

* Scale up before the application starts struggling (or scale down and save $$)
* Catch a memory leak ahead of time (lookin' at you Java!) 
* Validate that an application change has indeed made the application 20% faster over the last 2 weeks

### Examples

[Node Exporter](https://grafana.sofree.us/d/rYdddlPWk/node-exporter-full)

[Blackbox Exporter](https://grafana.sofree.us/d/xtkCtBkiz/prometheus-blackbox-exporter)

[Prometheus 2.0 Stats](https://grafana.sofree.us/d/5TYdZe3Zk/prometheus-2-0-stats)

[Grafana metrics](https://grafana.sofree.us/d/isFoa0z7k/grafana-metrics)

## Demo and Pair

### Get some data in Grafana and preview some dashboards

1. Import Prometheus Data Source (change localhost to prometheus:9090)
2. Import Dashboards --> Prometheus, Grafana
3. Import Node Exporter (1860)
4. Import Blackbox Exporter (7587)

[Grafana Visualizations](https://grafana.com/docs/grafana/latest/panels/visualizations/)

[Golden Grot Awards](https://grafana.com/golden-grot-awards/)

### Additional Info

* Lots of supported datasources! https://grafana.com/grafana/plugins?type=datasource
