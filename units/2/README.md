# Blackbox Exporter

![blackbox_exporter](blackbox_exporter.png)


## Talk

Sometimes, blackbox monitoring makes sense:

- You can't stick an exporter on: CloudFlare's DNS, your ISP's router, your partner's SFTP server, or your refrigerator, but you still want to watch them.
- Sometimes you want to see the service 'as the customer sees it'.

For situations like this, we have Blackbox Exporter!

The metrics-gathering is a little different than a normal scrape of /metrics.
1. Prometheus sends blackbox_exporter a get for specially crafted URI.
2. blackbox_exporter unpacks the URI into what it should probe.
3. blackbox_exporter returns a page of metrics about the probe results.

## Demo

Let's monitor HTTP on www.sofree.us and SSH on blackberry.sofree.us for up-ness.

### A quick ad-hoc probe

From Blackbox Exporter Web UI, click 'Probe prometheus.io for http_2xx'. Note that it fails (last line). Something wonky with CloudFlare? Hack the URI to www.sofree.us. Note that it succeeds.

### A periodic probe

Configure Prometheus to watch HTTP on www.sofree.us and SSH on blackberry.sofree.us

```bash
cd /srv/mad-stack/
vi prometheus/prometheus.yml
# or install and use the editor of your choice
```

Add the following jobs to the scrape_configs. Make sure to replace $MACGUFFIN in both sections.

```yaml
  - job_name: 'blackbox_ssh'
    metrics_path: /probe
    params:
      module: [ssh_banner]
    static_configs:
    - targets:
      - blackberry.sofree.us:22
      - $MACGUFFIN.sofree.us:22
    relabel_configs:
    - source_labels: [__address__]
      target_label: __param_target
    - source_labels: [__param_target]
      target_label: instance
    - target_label: __address__
      replacement: blackbox_exporter:9115

  - job_name: 'blackbox_http'
    metrics_path: /probe
    params:
      module: [http_2xx]
    static_configs:
    - targets:
        - http://www.sofree.us
        - http://$MACGUFFIN.sofree.us
    relabel_configs:
    - source_labels: [__address__]
      target_label: __param_target
    - source_labels: [__param_target]
      target_label: instance
    - target_label: __address__
      replacement: blackbox_exporter:9115
```

Caution: All '- job_name:' must be indented the same amount. It doesn't matter how much, as long as it matches.

Reboot the Prometheus container to make it pick up the new config.

```bash
docker-compose restart prometheus
docker-compose logs prometheus
```

### Checking probe results

In Blackbox Exporter, not the "Recent Probes" section.

In Prometheus, query stats like:
- probe_success
- probe_duration_seconds
- probe_http_uncompressed_body_length

## Pair (same exercise as Demo)
