# Monitoring, Alerting, and Dashboards with Prometheus, Grafana, and friends

## Unit 0: Overview and Infrastructure

- What is metrics-based monitoring, and why is it better than check-based monitoring?
- What are the components of the "MAD" stack?
- Start your "MAD" stack!

## Unit 1: Whitebox Monitoring

- Node and other exporters
- Explore PromQL

## Unit 2: Blackbox Monitoring

- Blackbox exporter

## Unit 3: Alerting

- Connect AlertManager to Mattermost
- Generate an alert :(
- Fix the alert!

## Unit 4: Grafana

- Add Prometheus Data Source
- Import [Node Exporter Full](https://grafana.com/grafana/dashboards/1860) dashboard
- Sharing dashboards


## Further Reading

- [SFS SysOps](https://gitlab.com/sofreeus/sfs-sysops/) Look in the monitoring or MAD-PGAF directory.
