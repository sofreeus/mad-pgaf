# Tools for the Teacher

## Notes

We expect the participants to have a Fuga instance (based on apt) to do the exercises. Those could be set up with the directions below, or with [GLADOS](https://gitlab.com/sofreeus/glados/-/blob/main/HOWTO.md).

## Before class

1. [Configure Mattermost](configure-mattermost.md) with a webhook to recieve notifications in the class channel
* Recommended: Pre-stage plenty of learner machines with MacGuffin names and DNS
* Test the materials

## At delivery time

1. If you pre-staged learner machines, assign them to learners
2. Otherwise, have learners create machines and create DNS records '*.$MACGUFFIN.sofree.us'

We have some bootstrap problems:

- The learners need `git-a-class` from the class material to clone the class material. I think it's safe to expect the learners to have a browser they can use to see these materials. So they can download the file and scp it to their instance. If they can't manage that you'll have to improvise 😄.
- The learners can set up their instance using GLADOS but it does not do DNS entries (yet).
  - Plan to collect macguffin/IP pairs from the students to enter in Gandi.
  - In Gandi, create an A record for $MACGUFFIN.sofree.us and a CNAME for *.$MACGUFFIN.sofree.us.

### DNS Example Records

... in sofree.us zone ...

```
*.alicorn 300 IN CNAME alicorn
*.dla 300 IN CNAME dla
alicorn 300 IN A 81.24.6.166
dla 300 IN A 81.24.7.58
```

### How to set up the infrastructure on Fuga Cloud

(Not sure what's needed to get these working.)
<details open>
Authenticate to Fuga Cloud

```bash
source fuga/openrc.sh
openstack image list
openstack flavor list
openstack server list
```

Optionally, get a fresh Linux image and upload it to Glance as an image.

https://cloud.debian.org/images/cloud/

```bash
openstack image create \
    --container-format bare \
    --disk-format qcow2 \
    --property hw_disk_bus=scsi \
    --property hw_scsi_model=virtio-scsi \
    --property os_type=linux \
    --property os_distro=debian \
    --property os_admin_user=debian \
    --property os_version='10.9.1' \
    --file  ~/Downloads/debian-10-openstack-amd64.qcow2 \
    debian-10-openstack-amd64
```

Create security group and key

```bash
./create-key
./create-security-group
```

Create machines

```bash
for FN in MACGUFFINS fuga.* create-fuga-servers; do echo $FN:; cat $FN; done
# if it all looks good, run
./create-fuga-servers
```

Add Teachers as admins to machines using GitLab usernames and authorized keys

```bash
./openstack_inventory > inventory
ansible all --list
ssh-add mad-pgaf-key # add default key
./add-admins debian all mad-pgaf-key
```

Remove built-in/default admins

```bash
ssh-add # add own key
ansible-playbook add-admins.yml --extra-vars='target=all'
```

# Ideas
- This infra methodology could be made generic and useful for all classes
- add admins?
- add DNS? (use Gandi API)
- map admins to servers, somehow
